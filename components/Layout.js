import React, { Component } from "react";
import { Container } from "native-base";
import AppHeader from "./Header";

class Layout extends Component {
  render() {
    const {
      children,
      searchQuery,
      handleChangeSearch,
      searchImages
    } = this.props;
    return (
      <Container>
        <AppHeader
          searchQuery={searchQuery}
          handleChangeSearch={handleChangeSearch}
          searchImages={searchImages}
        />
        {children}
      </Container>
    );
  }
}

export default Layout;
