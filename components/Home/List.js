import React, { Component } from "react";
import { View, Text } from "native-base";
import { FlatList, Image, StyleSheet } from "react-native";
import LoadMoreButton from "./LoadMoreButton";

class List extends Component {
  renderItem = item => {
    return (
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{ uri: item.urls.thumb }} />
      </View>
    );
  };
  renderStub = photos => {
    return (
      photos.length === 0 && (
        <View style={styles.empty}>
          <Text style={styles.emptyText}>Oops , nothing was found</Text>
        </View>
      )
    );
  };
  render() {
    const { photos, loadMorePhotos, loadMore } = this.props;
    return (
      <View>
        {photos && this.renderStub(photos)}
        {photos && (
          <View>
            <FlatList
              ref={ref => {
                this.listRef = ref;
              }}
              numColumns={2}
              data={photos}
              renderItem={({ item }) => this.renderItem(item)}
              keyExtractor={(item, index) => index.toString()}
            />
            {photos.length !== 0 ? (
              <LoadMoreButton
                loadMorePhotos={loadMorePhotos}
                loadMore={loadMore}
              />
            ) : (
              <Text />
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    flexDirection: "column",
    margin: 1
  },
  image: {
    width: "100%",
    height: 200
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10
  },
  emptyText: {
    fontSize: 18
  }
});

export default List;
