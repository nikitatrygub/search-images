import React from "react";
import { StyleSheet } from "react-native";
import { Container, Body, Text, Button } from "native-base";

const ErrorView = ({ error, refresh }) => (
  <Container
    style={{ padding: 10, justifyContent: "center", alignItems: "center" }}
  >
    <Text style={styles.errorMessage}>{error}</Text>
    <Body style={{ flexDirection: "row" }}>
      <Button style={styles.refreshButton} onPress={() => refresh()}>
        <Text>Refresh</Text>
      </Button>
    </Body>
  </Container>
);

const styles = StyleSheet.create({
  errorMessage: {
    marginTop: 20,
    marginBottom: 20
  },
  refreshButton: {
    width:'50%',
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#000000"
  }
});

export default ErrorView;
