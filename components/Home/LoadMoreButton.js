import React from "react";
import { StyleSheet } from "react-native";
import { Button, Text, Body } from "native-base";

const LoadMoreButton = ({ loadMorePhotos, loadMore }) => (
  <Body style={{ flexDirection: "row" }}>
    <Button
      style={styles.loadMoreButton}
      disabled={loadMore}
      onPress={() => loadMorePhotos()}
    >
      <Text>Load more</Text>
    </Button>
  </Body>
);

const styles = StyleSheet.create({
  loadMoreButton: {
    width:"50%",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 40,
    justifyContent: "center",
    backgroundColor:"#000000"
  }
});

export default LoadMoreButton;
