import React from "react";
import { StyleSheet, Keyboard } from "react-native";
import { Header, InputGroup, Input, Button, Icon } from "native-base";

const AppHeader = ({ searchQuery, searchImages, handleChangeSearch }) => (
  <Header
    searchBar
    rounded
    style={styles.header}
    androidStatusBarColor="#000000"
  >
    <InputGroup style={styles.inputGroup}>
      <Input
        value={searchQuery}
        type="text"
        onChangeText={text => handleChangeSearch(text)}
        placeholder="Search"
        style={styles.search}
        onSubmitEditing={() => Keyboard.dismiss()}
      />
      <Button
        style={styles.searchButton}
        onPress={() => {
          searchImages();
          Keyboard.dismiss();
        }}
      >
        <Icon name="ios-search" />
      </Button>
    </InputGroup>
  </Header>
);

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    height: 70,
    backgroundColor: "#ffffff",
    borderBottomColor: "#eeeeee",
    borderBottomWidth: 1
  },
  search: {
    borderColor: "#cdcdcd",
    borderWidth: 1,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    paddingLeft: 10
  },
  inputGroup: {
    flex: 1,
    backgroundColor: "#fff",
    height: 50,
    marginTop: 10,
    borderRadius: 10
  },
  searchButton: {
    height: 50,
    width: 80,
    justifyContent: "center",
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: "#000000"
  }
});

export default AppHeader;
