import { BASE_API_ENDPOINT, API_KEY } from "../env";

export const fetchImages = async (page, query) => {
  const isSearchQuery = query.length > 0 ? true : false;
  const buildUrl = () => {
    const params = {
      resource: "photos",
      query: `&query=${query.length > 0 ? query : ""}`,
      queryPrefix: `${query.length > 0 ? `search/` : ""}`,
      page: `?page=${page ? page : 1}`
    };

    const url = `${BASE_API_ENDPOINT +
      params.queryPrefix +
      params.resource +
      params.page +
      params.query}`;
    return url;
  };

 
  const resp = await fetch(buildUrl(), {
    mehtod: "GET",
    headers: {
      Authorization: `Client-ID ${API_KEY}`,
      "Content-Type": "application/json"
    }
  });
  if (resp.ok) {
    const images = await resp.json();
    if (isSearchQuery) {
      return images.results;
    }
    return images;
  } else {
    const errorMessage = await resp.json();
    throw new Error(errorMessage.errors[0]);
  }
};
