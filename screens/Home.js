import React, { Component } from "react";
import { Content, Spinner } from "native-base";
import { fetchImages } from "../api/fetchImages";
import List from "../components/Home/List";
import Layout from "../components/Layout";
import ErrorView from "../components/Home/ErrorView";

class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.state = initialState = {
      photos: null,
      loading: true,
      error: false,
      loadMore: false,
      searchQuery: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.getPhotos();
  }

  getPhotos = async () => {
    const { searchQuery } = this.state;
    try {
      const images = await fetchImages(1, searchQuery);
      this.setState({
        loading: false,
        photos: images
      });
    } catch (e) {
      this.setState({
        loading: false,
        error: e.message
      });
    }
  };

  loadMorePhotos = async () => {
    const { photos, searchQuery } = this.state;
    this.page = this.page + 1;
    this.setState({
      loadMore: true
    });
    try {
      const images = await fetchImages(this.page, searchQuery);
      this.setState({
        loading: false,
        photos: [...photos, ...images],
        loadMore: false
      });
    } catch (e) {
      this.setState({
        loading: false,
        error: e.message,
        loadMore: false
      });
    }
  };

  handleChangeSearch = value => {
    this.setState({
      searchQuery: value
    });
  };

  refresh = () => {
    this.setState(initialState);
    this.getPhotos();
  };

  render() {
    const { photos, loading, error, loadMore, searchQuery } = this.state;
    return (
      <Layout
        handleChangeSearch={this.handleChangeSearch}
        searchQuery={searchQuery}
        searchImages={this.getPhotos}
      >
        {loading && <Spinner color="blue" />}
        {error && <ErrorView error={error} refresh={this.refresh} />}
        <Content style={{ padding: 10 }}>
          <List
            photos={photos}
            loadMorePhotos={this.loadMorePhotos}
            loadMore={loadMore}
          />
        </Content>
      </Layout>
    );
  }
}

export default MainScreen;
