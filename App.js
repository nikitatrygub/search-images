import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import MainScreen from "./screens/Home";

const Stack = createStackNavigator(
  {
    Home: {
      screen: MainScreen
    }
  },
  {
    initialRouteName: "Home"
  }
);

const AppContainer = createAppContainer(Stack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
